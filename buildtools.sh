#! /usr/bin sh

apk add wget

apk add git

wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar

mkdir /root/output

java -jar BuildTools.jar --rev $minecraft_env --output-dir ../output

rm -rf /root/build/*

cd ../output

echo "eula=true" > eula.txt

wget -O server.properties https://server.properties/

sed -i 's/online-mode=true/online-mode=false/g' server.properties

sed -i 's/server-ip=/server-ip=0.0.0.0/g' server.properties

echo "done"
