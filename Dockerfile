FROM openjdk:17-alpine
ARG minecraft_version
ENV minecraft_env=$minecraft_version
USER root
COPY buildtools.sh /root/build/buildtools.sh
WORKDIR /root/build
CMD ["/bin/sh","./buildtools.sh"]